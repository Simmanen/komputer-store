//get dom elements
const displayWorkMoney = document.getElementById("display-pay");
const displayBankMoney = document.getElementById("money-in-bank");
const displayLoan = document.getElementById("display-loan");
const payLoanButton = document.getElementById("btn-pay-loan");
const hideLoan = document.getElementById("hide-loan");
const computersElement = document.getElementById("select-computers");
const priceElement = document.getElementById("computer-price");
const specElement = document.getElementById("computer-specs");
const descriptionElement = document.getElementById("computer-desc");
const titleElement = document.getElementById("computer-title");
const imageElement = document.getElementById("image-computer");
const buyComputerButton = document.getElementById("btn-buy-computer");

//eventlisteners
document.getElementById("btn-work").addEventListener("click", addMoney);
document.getElementById("btn-deposit-money").addEventListener("click", deposit);
document.getElementById("btn-get-loan").addEventListener("click", getLoan);
document.getElementById("btn-pay-loan").addEventListener("click", payLoan);
document.getElementById("btn-buy-computer").addEventListener("click", handleBuy);

//adds 100 each time you click work
let workMoney = 0;
displayWorkMoney.innerText = `Money: ${workMoney}kr`;

function addMoney(){    
    workMoney += 100;
    displayWorkMoney.innerText = `Money: ${workMoney}kr`;
}

//updates the bank balance
let bankMoney = 0;
displayBankMoney.innerText = `Balance: ${bankMoney}kr`;

function deposit(){
    if (remindingLoan > workMoney*0.1){
        bankMoney += workMoney*0.9;
        remindingLoan -= workMoney*0.1;
        displayLoan.innerText = `Loan:   ${remindingLoan}kr`;
        displayBankMoney.innerText = `Balance: ${bankMoney}kr`;
        workMoney = 0;
        displayWorkMoney.innerText = `Money: ${workMoney}kr`;
    }else{
        bankMoney += workMoney;
        displayBankMoney.innerText = `Balance: ${bankMoney}kr`;
        workMoney = 0;
        displayWorkMoney.innerText = `Money: ${workMoney}kr`;
    };
}

function getLoan(){
    let askLoan = prompt("Amount of money you wanna loan?");
    askLoan = parseInt(askLoan);  
    if (askLoan <= bankMoney*2 && remindingLoan <= 0){
        remindingLoan = askLoan;
        bankMoney += askLoan;
        displayLoan.innerText = `Loan:   ${remindingLoan}kr`;
        displayBankMoney.innerText = `Balance: ${bankMoney}kr`;
        if (remindingLoan <= 1){
            payLoanButton.style.display = "none";   
            hideLoan.style.display = "none";
            }
            else{
                payLoanButton.style.display = "block";
                hideLoan.style.display = "block";               
            }
    }
    else{
        alert("You can not take up a loan at the moment");
    }
};

//Shows pay loan button if you got a loan
let remindingLoan = 0;
if (remindingLoan <= 0){
    payLoanButton.style.display = "none";   
    hideLoan.style.display = "none";
    }
    else{
        payLoanButton.style.display = "block";
        hideLoan.style.display = "block";
    };

//pays down the loan if you got enough money
function payLoan(){
    if (workMoney >= remindingLoan){
        workMoney -= remindingLoan;
        remindingLoan = 0;
        displayWorkMoney.innerText = `Money: ${workMoney}kr`;
        displayLoan.innerText = `Loan:   ${remindingLoan}kr`;
        payLoanButton.style.display = "none";   
        hideLoan.style.display = "none";
    }else{
        alert("Not enough money in your account");
    };
};   

// gets the api
let computers = [];

fetch("https://hickory-quilled-actress.glitch.me/computers")
    .then(response => response.json())
    .then(data => computers = data)
    .then(computers => addComputersToList(computers));

const addComputersToList = (computers) => {
    computers.forEach(x => addComputerToList(x));
    descriptionElement.innerText = computers[0].description;
    specElement.innerText = computers[0].specs;
    priceElement.innerText = `Price: ${computers[0].price}kr`;
    titleElement.innerText = computers[0].title;
    imageElement.src = ("https://hickory-quilled-actress.glitch.me/") + computers[0].image;
};

const addComputerToList = (computer) => {
    const computerElement = document.createElement("option");
    computerElement.value = computer.id;
    computerElement.appendChild(document.createTextNode(computer.title));
    computersElement.appendChild(computerElement);
};

const computerListChange = e => {
    const selectedComputer = computers[e.target.selectedIndex];
    descriptionElement.innerText =selectedComputer.description;
    specElement.innerText = selectedComputer.specs;
    priceElement.innerText = `Price: ${selectedComputer.price}kr`;
    titleElement.innerText = selectedComputer.title;
    imageElement.src = ("https://hickory-quilled-actress.glitch.me/") + selectedComputer.image;
    let images = document.getElementsByTagName("img");
    for (let i = 0; i < images.length; i++){
        checkImage(images[i]);
    }
};

// handles the buy
computersElement.addEventListener("change", computerListChange);

function handleBuy (){
    const selectedComputer = computers[computersElement.selectedIndex];
    const thisPrice = selectedComputer.price;
    const thisPc = selectedComputer.title;
    if (thisPrice <= bankMoney){
        alert(`Congrats you bought: ${thisPc}!`);
        bankMoney -= thisPrice;
        displayBankMoney.innerText = `Balance: ${bankMoney}kr`;
    }else{
        alert("Sorry you do not have enough money in the bank!");
    }  
};
//on error checks the image extension and replaces it
function checkImage(img){
    let imgExtensions = [".png", ".jpg", ".gif"];
    let currentImgExt = 0;
    
    img.addEventListener("error", function(){
        if(currentImgExt >= imgExtensions.length){
          newSrc;
          return;
        }
        var src = img.src;
        var newSrc = src.substring(0, src.lastIndexOf(".")) + imgExtensions[currentImgExt];
            currentImgExt++;
            img.src = newSrc;
    })
};




 

    



